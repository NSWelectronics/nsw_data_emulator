#ifndef RECEIVER_H
#define RECEIVER_H

//std/stl
#include <string>
#include <cstdint>
#include <sstream>
#include <vector>
#include <tuple>

//boost
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/array.hpp>

//nsw
#include "decoder_base.h"

typedef std::tuple<std::string, std::vector<uint8_t>, uint32_t> IPDataArray8;
typedef std::tuple<std::string, std::vector<uint32_t>, uint32_t> IPDataArray;
#define MAX_UDP_LEN 250
typedef boost::array<uint8_t, MAX_UDP_LEN> RawDataArray8;
typedef boost::array<uint32_t, MAX_UDP_LEN> RawDataArray;

class Receiver {

    public :
        Receiver();
        virtual ~Receiver();

        void listen_to_port(int port) { m_port = port; }
        void netinet(bool doit) { m_do_netinet = doit; }
        void u8(bool doit) { m_do_u8 = doit; }

        void listen();
        void listen_callback();
        void read_data(const boost::system::error_code error,
                            std::size_t size);
        void read_data8(const boost::system::error_code error,
                            std::size_t size);
        void handle_data(IPDataArray data);
        void handle_data8(IPDataArray8 data);

        void print_next_decoding() { m_print_next_decoding = true; }

        void print_last_packet();
        uint8_t trailer() { return unsigned(m_trailer); }

        
        uint32_t message_count() { return m_message_count; } 
        bool continue_listening() { return m_continue_listening; }
        void stop();

    private :
        bool m_continue_listening;
        int m_port;
        bool m_do_netinet;
        bool m_do_u8;
        bool m_print_next_decoding;

        boost::shared_ptr<boost::asio::io_service> m_io_service;
        boost::shared_ptr<boost::asio::io_service::work> m_work;
        boost::shared_ptr<boost::asio::ip::udp::socket> m_socket;
        boost::thread_group m_thread_group;
        boost::asio::ip::udp::endpoint m_endpoint;
        uint32_t m_message_count;
        RawDataArray m_data_buffer;
        RawDataArray8 m_data_buffer8;

        std::vector<uint32_t> m_last_packet;
        std::vector<uint8_t> m_last_packet8;
        std::vector< std::vector<uint8_t> > m_last_packets8;
        uint32_t m_trailer;
        uint8_t m_trailer8;

        // decoder
        nsw::DecoderBase decoder;

};

#endif
