#include "receiver.h"

//std/stl
#include <iostream>
using namespace std;

void help()
{
    cout << "-----------------------------------------------" << endl;
    cout << "receiver" << endl;
    cout << "" << endl;
    cout << "Options:" << endl;
    cout << "  -p|--port    port to bind the receiver to" << endl;
    cout << "  --netinet    convert integers from network-byte-order to host-byte-order" << endl;
    cout << "  --u8         unpack data from the network into 8-bit arrays instead of 32-bit arrays" << endl;
    cout << "               (if you do '--u8', '--netinet' will not have an effect)" << endl;
    cout << "  -h|--help    print this help message" << endl; 
    cout << endl;
    cout << "Usage:" << endl;
    cout << " ./receiever [options]" << endl;
    cout << endl;
    cout << "Running options:" << endl;
    cout << "  p+<enter>    print the last data packet received" << endl;
    cout << "  n+<enter>    print the number of packets received" << endl;
    cout << "  d+<enter>    print next decoding" << endl;
    cout << "  q+<enter>    kill the receiver and exit" << endl;
    cout << "-----------------------------------------------" << endl;
}

int main(int argc, char* argv[])
{
    cout << "Receiver application" << endl;

    int optin = 1;

    int listen_port = 1234;
    bool do_netinet = false;
    bool do_u8; // unpack things as uint8_t vs uint32_t

    while(optin < argc) {
        string in = argv[optin];
        if      (in == "-p" || in == "--port") { listen_port = atoi(argv[++optin]); }
        else if (in == "--netinet") { do_netinet = true; }
        else if (in == "--u8" ) { do_u8 = true; }
        else if (in == "-h" || in == "--help" ) { help(); return 0; }
        else {
            cout << "Receiver    ERROR Unknown command line argument '" << in << "' provided, exiting" << endl;
            help();
            return 1;
        }
        optin++;
    } // while

    Receiver rcvr;
    rcvr.listen_to_port(listen_port);
    rcvr.netinet(do_netinet);
    rcvr.u8(do_u8);
    rcvr.listen();

    char input = 'o';
    do {
        switch(input) {
            case 'q' :
                cout << "receiver quitting" << endl;
                break;
            case 'p' :
                rcvr.print_last_packet();
                break;
            case 'n' :
                cout << "receiver count : " << rcvr.message_count() << endl;
                break;
            case 't' :
                cout << "trailer : 0x" << std::hex << rcvr.trailer() << std::dec << endl;
                break;
            case 'd' :
                rcvr.print_next_decoding();
                break;
            default :
                cout << "Type one of the following:\n"
                     << "  p  to show a received fragment\n"
                     << "  n  print current message count\n"
                     << "  q  to quit\n";
                break;
        } // switch
        std::cin >> input;


    } while (input != 'q');

    rcvr.stop();



    return 0;
}
