#include "receiver.h"

//std/stl
#include <iostream>
#include <vector>
#include <bitset>
#include <mutex>
using namespace std;

//network
#include <netinet/in.h>

//boost
#include <boost/system/error_code.hpp>
using boost::asio::ip::udp;
namespace ip = boost::asio::ip;

// decoding
#include "decode_type.h"
#include "vmm_packet.h"

const uint32_t SOP = 0x3c;
const uint32_t EOP = 0xdc;
const int NPRINT = 5;

std::mutex packet_mutex;

///////////////////////////////////////////////////////////////////
Receiver::Receiver() :
    m_continue_listening(true),
    m_port(1234),
    m_do_netinet(false),
    m_do_u8(false),
    m_print_next_decoding(false),
    m_message_count(0),
    m_trailer(0x0)
{
    cout << "Receiver says hello" << endl;

    m_io_service = boost::shared_ptr<boost::asio::io_service>(new boost::asio::io_service);
    m_work = boost::shared_ptr<boost::asio::io_service::work>(new boost::asio::io_service::work(*m_io_service));

    // configure the decoder
    decoder.set_decoding(nsw::DecodeType::ROC);
}
///////////////////////////////////////////////////////////////////
Receiver::~Receiver()
{
    cout << "receiver says goodbye" << endl;
}
///////////////////////////////////////////////////////////////////
void ReceiveThread(boost::shared_ptr<boost::asio::io_service> io_service)
{
    cout << "[" << boost::this_thread::get_id() << "] ReceiveThread start" << endl;
    io_service->run();
}
///////////////////////////////////////////////////////////////////
void Receiver::listen()
{
    m_socket = boost::shared_ptr<udp::socket>(new udp::socket(*m_io_service,
                    ip::udp::endpoint(ip::udp::v4(), m_port)));
    m_message_count = 0;

    if(m_socket->is_open()) {
        cout << "receiver udp socket is open" << endl;
    }
    else {
        cout << "receiver udp socket is not open, error" << endl;
        return;
    }

    m_thread_group.create_thread(boost::bind(ReceiveThread, m_io_service));

    m_io_service->post(boost::bind(&Receiver::listen_callback, this));

}
///////////////////////////////////////////////////////////////////
void Receiver::listen_callback()
{
    if(m_do_u8) {
        m_socket->async_receive_from(
            boost::asio::buffer(m_data_buffer8), m_endpoint,
            boost::bind(&Receiver::read_data8, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred)
        );
    }
    else {
        m_socket->async_receive_from(
            boost::asio::buffer(m_data_buffer), m_endpoint,
            boost::bind(&Receiver::read_data, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred)
        );
    }

}
///////////////////////////////////////////////////////////////////
void Receiver::read_data(const boost::system::error_code /*error*/,
    std::size_t data_length /*bytes*/)
{
    if(!continue_listening()) return; // stop the callback

    // ip address of sender
    string ip = m_endpoint.address().to_string();
    size_t length = 4*data_length;

    uint32_t sop_mask = 0x000000ff;
    uint32_t eop_mask = 0xff000000;

    if( (m_data_buffer.at(0) & sop_mask) != SOP) {
        cout << "Receiver::read_data    WARNING Expected SOP, got 0x" << std::hex << (m_data_buffer.at(0) & sop_mask) << std::dec << endl;
    }
    vector<uint32_t> incoming;
    size_t n_added = 0;
    for(auto& x : m_data_buffer) {
        if(n_added >= data_length) break;
        if(m_do_netinet) {
            incoming.push_back(ntohl(x));
            //if( ( ntohl(x) & eop_mask ) == EOP) break;
        }
        else {
            incoming.push_back(x);
            //if( ( x & eop_mask ) == EOP) break;
        }
        n_added += 4; // data_length is in bytes, here we process 32-bit at a time
    } // x

    IPDataArray darr(ip, incoming, incoming.size());
    handle_data(darr);
    m_message_count++;
    // continue listening
    listen_callback();
}
///////////////////////////////////////////////////////////////////
void Receiver::read_data8(const boost::system::error_code /*error*/,
        std::size_t data_length /*bytes*/)
{

    if(!continue_listening()) return; // stop the callback

    // ip address of sender
    string ip = m_endpoint.address().to_string();
    m_trailer = unsigned(m_data_buffer8.at(m_data_buffer8.size()-1));

    //if(unsigned(m_data_buffer8.at(0)) != SOP)
    //    cout << "Receiver::read_data8    WARNING Expected SOP, got " << unsigned(m_data_buffer8.at(0)) << endl;
    //if(m_data_buffer8.at(3) == SOP) {
    //    cout << "Recever::read_data8    SOP is at 4th byte position, octet ordering reversed?" << endl;
    //}

    //cout << "Receiver::read_data8    header : ";
    //int n_check = 0;
    //for(auto& x : m_data_buffer8) {
    //    if(n_check>=5) break;
    //    cout << std::bitset<8>(unsigned(x)) << " "; 
    //    n_check++;
    //}
    //cout << endl;
    

    vector<uint8_t> incoming;
    int ffcount=0;

    size_t n_added = 0;
    for(auto& x : m_data_buffer8) {
        if(n_added >= data_length) break;
        // if we unpack as 8-bit objects, do not use ntohl as it turns them into 32-bit words
        //if(m_do_netinet) {
        //    cout << "push back 8 " << std::hex << unsigned(ntohl(x)) << endl;
        //    incoming.push_back(ntohl(x));
        //    //if(unsigned(ntohl(x)) == EOP) break;
        //}
        //else {
            incoming.push_back(x);
            //if(unsigned(x) == EOP) break;
        //}
        n_added++;
    }

    IPDataArray8 darr(ip, incoming, incoming.size());
    handle_data8(darr);
    m_message_count++;
    // continue listening
    listen_callback();
}
///////////////////////////////////////////////////////////////////
void Receiver::handle_data(IPDataArray datapacket)
{
    string ip = std::get<0>(datapacket);
    vector<uint32_t> data = std::get<1>(datapacket);
    uint32_t packet_length = std::get<2>(datapacket);

    if(data.at(0) != SOP) {
        cout << "WARNING Expected SOP not found, got " << std::hex << unsigned(data.at(0)) << std::dec << endl;
    }
    if(data.at(packet_length-1) != EOP) {
        cout << "WARNING Expected EOP not found, got " << std::hex << unsigned(data.at(packet_length-1)) << std::dec << endl;
    }

    std::lock_guard<std::mutex> guard(packet_mutex);
    m_last_packet.clear();
    uint32_t eop_mask = 0xff000000;
    for(auto x : data) { m_last_packet.push_back(x); } // if( (x & eop_mask) == EOP) break; }
}
///////////////////////////////////////////////////////////////////
void Receiver::handle_data8(IPDataArray8 datapacket)
{
    string ip = std::get<0>(datapacket);
    vector<uint8_t> data = std::get<1>(datapacket);
    uint32_t packet_length = std::get<2>(datapacket);

    std::lock_guard<std::mutex> guard(packet_mutex);
    m_last_packet8.clear();
    for(auto x : data) {  m_last_packet8.push_back(x); } //if(unsigned(x)==EOP) break; }
    if(m_last_packets8.size()==NPRINT) m_last_packets8.erase(m_last_packets8.begin());
    m_last_packets8.push_back(m_last_packet8);


    // decode
    if(!decoder.collect(data)) {
        cout << "Receiver    data collection error" << endl;
        return;
    }
    nsw::VMMPacket vmm_packet = decoder.decode();

    //if(vmm_packet.is_null) {
    //    cout << "null packet    sop = " << std::hex << unsigned(vmm_packet.header.null_sop) << "  eop = " << std::hex << unsigned(vmm_packet.header.null_eop) << endl;
    //}

    if(m_print_next_decoding) {
        m_print_next_decoding = false;
        vmm_packet.print();
    }

}
///////////////////////////////////////////////////////////////////
void Receiver::print_last_packet()
{
    if(m_do_u8) {
        cout << "last " << NPRINT << " packet(s) received:\n"; //0x ";
        std::lock_guard<std::mutex> guard(packet_mutex);
        size_t outer = m_last_packets8.size();
        for(size_t i = outer; i > 0; i--) {
            vector<uint8_t> inner = m_last_packets8.at(i-1);
            cout << "[" << i << "] 0x ";
            for(size_t j = inner.size(); j > 0; j--) {
                cout << std::hex << unsigned(inner.at(j-1)) << " ";
            }
            cout << std::dec << "\n";
        } // i
    }
    else {
        cout << "last packet received: 0x ";
        std::lock_guard<std::mutex> guard(packet_mutex);
        for(auto x : m_last_packet) cout << std::hex << x << " ";
        cout << std::dec << "\n";
    }
}
///////////////////////////////////////////////////////////////////
void Receiver::stop()
{
    m_continue_listening = false;
    m_io_service->stop();
    m_thread_group.join_all();
}
