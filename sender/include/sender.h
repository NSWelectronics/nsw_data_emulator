#ifndef SENDER_H
#define SENDER_H

//std/stl
#include <string>
#include <cstdint>
#include <sstream>
#include <vector>
#include <fstream>

//boost
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

class Sender {

    public :
        Sender();
        virtual ~Sender();

        void handle_send(const boost::system::error_code& error, std::size_t bytes_transferred);

        void send_to_port(int port);
        void send_fixed();
        void send_fixed_callback();
        std::vector<uint8_t> data_to_byte_array(uint32_t data);
        void send_from_file(std::string input_file);
        void send_from_file_callback(std::string input_file);
        void print_last_packet();
        uint32_t message_count();
        bool continue_sending() { return m_continue_sending; }
        void stop();

    private :
        bool m_continue_sending;
        int m_port;
        boost::shared_ptr<boost::asio::io_service> m_io_service;
        boost::shared_ptr<boost::asio::io_service::work> m_work;
        boost::shared_ptr<boost::asio::ip::udp::socket> m_socket;
        boost::thread_group m_thread_group;
        boost::asio::ip::udp::endpoint m_endpoint;
        uint32_t m_message_count;
        std::vector<uint8_t> m_last_packet;
        std::ifstream* m_infile;


}; // class


#endif
