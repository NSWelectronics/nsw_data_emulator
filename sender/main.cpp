#include "sender.h"

// std/stl
#include <string>
#include <fstream>
#include <iostream>
using namespace std;


void help() {
    cout << "-----------------------------------------------" << endl;
    cout << "sender" << endl;
    cout << endl;
    cout << "Options:" << endl;
    cout << " -p|--port    port to send data to [default:1234]" << endl;
    cout << " -f|--file    text file to read data from (each line must be 32-bits in hex)" << endl;
    cout << " -h|--help    print this help message" << endl;
    cout << endl;
    cout << "Usage:" << endl;
    cout << " - Send fixed data pattern hardcoded:" << endl;
    cout << "     ./sender" << endl;
    cout << " - Send data from a text file:" << endl;
    cout << "     ./sender -f data.txt" << endl;
    cout << endl;
    cout << "Running options:" << endl;
    cout << "  p+<enter>    print the last data packet sent" << endl;
    cout << "  n+<enter>    print the number of packets sent" << endl;
    cout << "  q+<enter>    kill the sender and exit" << endl;
    cout << "-----------------------------------------------" << endl;
}

int main(int argc, char* argv[])
{
    cout << "Sender application" << endl;

    int optin = 1;

    bool read_from_file = false;
    string input_file = "";
    int port = 1234;

    while(optin < argc) {
        string in = argv[optin];
        if      (in == "-f" || in == "--file") { read_from_file = true; input_file = argv[++optin]; }
        else if (in == "-p" || in == "--port") { port = atoi(argv[++optin]); }
        else if (in == "-h" || in == "--help") { help(); return 0; }
        else {
            cout << "Sender    ERROR Unknown command line argument '" << in << "' provided, exiting" << endl;
            help();
            return 1;
        }
        optin++;
    } // while


    Sender sender;
    sender.send_to_port(port);

    if(!read_from_file) {
        sender.send_fixed();
    }
    else if(read_from_file) {
        bool exists = std::ifstream(input_file).good();
        if(!exists) {
            cout << "Sender    ERROR Could not open input file '" << input_file << "', exiting" << endl;
            return 1;
        }
        sender.send_from_file(input_file);
    }

    char input;
    do {
        switch (input) {
            case 'q' :
                cout << "quitting" << endl;
                break;
            case 'p' :
                sender.print_last_packet();
                break;
            case 'n' :
                cout << "sender count : " << sender.message_count() << endl;
                break;
            default :
                cout << "Type one of the following:\n"
                     << "  p  to show a sent fragment\n"
                     << "  n  print current message count\n"
                     << "  q  to quit\n";
                break;
        } // switch
        std::cin >> input;

    } while (input != 'q');

    sender.stop();




    return 0;
}
