#include "sender.h"

// std/stl
#include <iostream>
#include <vector>
#include <fstream>
#include <istream>
#include <mutex>
using namespace std;

std::mutex packet_mutex;

// boost
#include <boost/system/error_code.hpp>
using boost::asio::ip::udp;
namespace ip = boost::asio::ip;

const uint32_t SOP = 0x3c;
const uint32_t EOP = 0xdc;

///////////////////////////////////////////////////////////////////
Sender::Sender() :
    m_continue_sending(true),
    m_port(1234),
    m_message_count(0)
{
    m_io_service = boost::shared_ptr<boost::asio::io_service>(new boost::asio::io_service);
    m_work = boost::shared_ptr<boost::asio::io_service::work>(new boost::asio::io_service::work(*m_io_service));
    
}
///////////////////////////////////////////////////////////////////
void SendThread(boost::shared_ptr< boost::asio::io_service > io_service)
{
    //cout << "[" << boost::this_thread::get_id() << "] SendThread start" << endl;
    io_service->run();
}
///////////////////////////////////////////////////////////////////
void Sender::send_to_port(int port)
{
    m_port = port;
}
///////////////////////////////////////////////////////////////////
Sender::~Sender()
{
    cout << "Sender says goodbye" << endl;
}
///////////////////////////////////////////////////////////////////
void Sender::stop()
{

    m_continue_sending = false;
    m_io_service->stop();
    m_thread_group.join_all();
    
}
///////////////////////////////////////////////////////////////////
void Sender::print_last_packet()
{
    std::lock_guard<std::mutex> guard(packet_mutex);
    cout << "last packet: 0x";
    size_t size = m_last_packet.size();
    for(size_t i = size; i > 0; i--) {
        cout << std::hex << unsigned(m_last_packet.at(i-1)) << " ";
    }
    cout << std::dec << "\n";
}
///////////////////////////////////////////////////////////////////
uint32_t Sender::message_count()
{
    return m_message_count;
}
///////////////////////////////////////////////////////////////////
void Sender::send_fixed()
{
    m_socket = boost::shared_ptr<udp::socket>(new udp::socket(*m_io_service,
                        ip::udp::endpoint(ip::udp::v4(), 1236)));
    m_message_count = 0;

    if(m_socket->is_open()) {
        cout << "udp socket is open" << endl;
    }
    else {
        cout << "udp socket is not open, error" << endl;
        return;
    }


    
    string ip_address = "127.0.0.1";
    m_endpoint.address(ip::address::from_string(ip_address));
    m_endpoint.port(m_port);

    m_thread_group.create_thread(boost::bind(SendThread, m_io_service));

    m_io_service->post(boost::bind(&Sender::send_fixed_callback, this));

}
///////////////////////////////////////////////////////////////////
void Sender::send_fixed_callback()
{
    vector<uint32_t> data_to_send {
        SOP,
        0xfafafafa,
        0xcacacaca,
        0xadadadad,
        0xffffffff,
        EOP
    };
    for(auto x : data_to_send) {
        cout << "data to send : 0x" << std::hex << x << endl;
    }
    cout << std::dec << std::endl;

    while(continue_sending()) {
        std::lock_guard<std::mutex> guard(packet_mutex);
        m_last_packet.clear();
        for(auto x : data_to_send) {
            m_last_packet.push_back(x);
        }
        m_socket->async_send_to(boost::asio::buffer(data_to_send), m_endpoint,
                boost::bind(&Sender::handle_send, this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred));
        m_message_count++;
    }
}
///////////////////////////////////////////////////////////////////
void Sender::send_from_file(std::string input_file)
{
    m_socket = boost::shared_ptr<udp::socket>(new udp::socket(*m_io_service,
                        ip::udp::endpoint(ip::udp::v4(), 1236)));
    m_message_count = 0;

    if(!m_socket->is_open()) {
        cout << "udp socket is not open, error" << endl;
        return;
    }

    m_infile = new ifstream(input_file);

    string ip_address = "127.0.0.1";
    m_endpoint.address(ip::address::from_string(ip_address));
    m_endpoint.port(m_port);

    m_thread_group.create_thread(boost::bind(SendThread, m_io_service));
    m_io_service->post(boost::bind(&Sender::send_from_file_callback, this, input_file));

}
///////////////////////////////////////////////////////////////////
vector<uint8_t> Sender::data_to_byte_array(uint32_t data)
{
    vector<uint8_t> out;
    uint32_t dt = data;

    uint32_t mask8 = 0x000000ff;
    int number = 4;
    if(data <= 0xff) number = 1;
    else if(data <= 0xffff) number = 2;
    else if(data <= 0xffffff) number = 3;
    else if(data <= 0xffffffff) number = 4;
    for(int i = 0; i < number; i++) {
        uint8_t x = ((dt >> 8*i) & mask8);
        out.push_back(x);
    }
    return out;
}
///////////////////////////////////////////////////////////////////
void Sender::send_from_file_callback(std::string input_file)
{
    string dataline;
    //ifstream infile (input_file);
    uint32_t data;
    vector<uint32_t> data_to_send;

    vector<uint8_t> data_to_send8;
    vector<uint8_t> data8;

    if(m_infile->is_open()) {
        while(continue_sending()) {
            data_to_send.clear();
            data_to_send8.clear();
            std::lock_guard<std::mutex> guard(packet_mutex);
            m_last_packet.clear();
            while (getline(*m_infile, dataline)) {
                data8.clear();

                data = std::stoul(dataline, 0, 16);
                if(dataline=="dc") data8.push_back(EOP);
                else if(dataline=="3c") data8.push_back(SOP);
                else {
                    data8 = data_to_byte_array(data);
                }
                for(auto byte : data8) {
                    data_to_send8.push_back(byte);
                    if(byte==EOP) {
                        m_last_packet.clear();
                        for(auto x : data_to_send8) { m_last_packet.push_back(x); }
                        m_socket->async_send_to(boost::asio::buffer(data_to_send8), m_endpoint,
                                boost::bind(&Sender::handle_send, this,
                                boost::asio::placeholders::error,
                                boost::asio::placeholders::bytes_transferred));
                        m_message_count++;
                        data_to_send8.clear();
                    }
                }
                ////if(data==0xffffffff) {
                //if(data==EOP) {
                //    m_last_packet.clear();
                //    for(auto x : data_to_send) {  m_last_packet.push_back(x); }
                //    m_socket->async_send_to(boost::asio::buffer(data_to_send), m_endpoint,
                //            boost::bind(&Sender::handle_send, this,
                //            boost::asio::placeholders::error,
                //            boost::asio::placeholders::bytes_transferred));
                //    m_message_count++;
                //    data_to_send.clear();
                //} // found trailer, send it
            } // getline

            // go back to the beginning of the file to restart the loop
            m_infile->clear();
            m_infile->seekg(0, ios::beg);
            data_to_send8.clear();
        } // continue_sending
    } // file open
    m_infile->close();

}
///////////////////////////////////////////////////////////////////
void Sender::handle_send(const boost::system::error_code& /*error*/,
        std::size_t /*bytes_transferred*/)
{
}
